/**
 * 
 */

/**
 * @author ryankropp
 *
 */
public class Items {
	
	private String itemName;
	private String make;
	private double price;	
	
	Items() {
		this.stockNum = 0000;
		this.itemName = "";
		this.make = "";
		this.price = 0.00d;
	}
	
	Items(int stockNum, String itemName, String make, double price) {
		this.stockNum = stockNum;
		this.itemName = itemName;
		this.make = make;
		this.price = price;
	}
	
	private int stockNum;
	/**
	 * @return the stockNum
	 */
	public int getStockNum() {
		return stockNum;
	}

	/**
	 * @param stockNum the stockNum to set
	 */
	public void setStockNum(int stockNum) {
		this.stockNum = stockNum;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}

	/**
	 * @param make the make to set
	 */
	public void setMake(String make) {
		this.make = make;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Stock Number: " + stockNum +
				"\nProduct Name: " + itemName +
				"\nManufacturer: " + make +
				"\nPrice: $" + String.format("%.2f", price) + "\n";
	}
	
}
