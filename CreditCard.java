
public class CreditCard {
	
	private String issuer;
	private String cardNumber;
	private String expirationDate;
	
	public CreditCard(String iss, String number, String expire)
	{
		this.issuer = iss;
		this.cardNumber = number;
		this.expirationDate = expire;
		
		
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "CreditCard [issuer=" + issuer + ", cardNumber=" + cardNumber + ", expirationDate=" + expirationDate
				+ "]";
	}
	
	public void authorize()
	{
		System.out.println( Luhns.check_card_number(getCardNumber()));
		
		
	}
	
	

}
