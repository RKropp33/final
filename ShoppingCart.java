
import java.util.ArrayList;

import java.util.Scanner;


class ShoppingCart {

 
private	ArrayList<TangibleItem> cart;
private	  double cartTotal;
	
	public ShoppingCart() {
		this.cart = new ArrayList<TangibleItem>();
		this.cartTotal = 0.0d;
		
		
		
		
	}
	
	
	public void fillCart(ArrayList<TangibleItem> list)
	{
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		String search;
	
		 
		
		System.out.println("Enter item number or X when finished");
		search = keyboard.next();
	
		boolean done = false;
		while(!done) {	
			
	
			
			
			for(int i = 0;i < list.size(); i++  ) 
			{
				if((list.get(i).getStockNumber().indexOf(search)) != -1) {
				cart.add(list.get(i));
				cartTotal = cartTotal + list.get(i).getPrice();
				}
			    
			}

			String answer;
			answer = keyboard.next();
			if (answer.charAt(0) == 'x')
			{
				done = true;
			}
			else {
				done = false;
			    search = answer;	
			}
	}
		
	}
	
	
	
	public double getCartTotal() {
		
		return cartTotal;
		
	}
	
	
	
	public void showCart() {
		
		System.out.println("Your Shopping Cart\n" +
				
				"=========================\n");
		
		System.out.println("Items in your Cart:\n");
		
		cart.forEach(
				x ->{
					
					System.out.println("Purchase: " + x.getName() +" " + x.getMake()+" $"+x.getPrice());
					
				});
		
		System.out.println("Total: " + getCartTotal() +"$");                   
		
	}
	
}



 


