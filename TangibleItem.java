
public abstract class TangibleItem implements Item{
	
	
	String stockNumber;
	String name;
	String make;
	double price;
	
	
	
	public TangibleItem(String sn, String name, String make, double price)
	{
		
		this.stockNumber = sn;
		this.name = name;
		this.make = make;
		this.price = price;
		
		
		
		
		
	}
	
	public TangibleItem(TangibleItem s)
	{
		
		this.stockNumber = s.getStockNumber();
		this.name = s.getName();
		this.make = s.getMake();
		this.price = s.getPrice();
		
		
		
	}
	
	
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
	
	

}
