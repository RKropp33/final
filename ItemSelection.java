import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/**
 * 
 */

/**
 * @author ryankropp
 *
 */
public class ItemSelection {
	private static boolean loop = true;
	private static Scanner keyboard = new Scanner(System.in);
	
	public static String Product(int userInput) {
		String product = "";
		switch (userInput) {
		case 1:
			product = "Golf Clubs";
			break;
		case 2:
			product = "Golf Balls";
			break;
		case 3:
			product = "Golf Shirts";
			break;
		default:
			System.out.println("Invalid Team Selection\n");
			System.exit(0);
			break;
		}
		System.out.println(product + " For Sale:");
		return product;
	}
	
	public static ArrayList<Items> ProductList(String product) {
		ArrayList<Items> itemList = new ArrayList<>();
		
		try {
		// open file input stream
			BufferedReader reader = new BufferedReader(new FileReader("balls.txt"));

			// read file line by line
			String line = null;
			Scanner scanner = null;
			int index = 0;
			
			while ((line = reader.readLine()) != null) {
				Items item = new Items();
				scanner = new Scanner(line);
				scanner.useDelimiter(",");
				while (scanner.hasNext()) {
					String data = scanner.next();
					if (index == 0) 
						item.setStockNum(Integer.parseInt(data));
					else if (index == 1)
						item.setItemName(data);
					else if (index == 2) 
						item.setMake(data);
					else if (index == 3)
						item.setPrice(Double.parseDouble(data));
					else
						System.out.println("invalid data::" + data);
					index++;
				}
				index = 0;
				itemList.add(item);
			}
			
			//close reader
			reader.close();
			
			//System.out.println(itemList);	
		}
		
		catch (IOException e)   
		{  
		e.printStackTrace();  
		}
		return itemList;  
	}  // readEmails
}
