
public class Club extends TangibleItem{
	
	
	private double length;
	
	public Club(String sn, String name, String make, double price)
	{
		super(sn,name, make,price);
		this.length = 10;
		
	}
	
	
	
	@Override
	public String toString() {
		return "StockNumber=" + stockNumber + "\nname=" + name + "\nmake=" + make + "\nprice="
				+ price + "$\nlenth=" + length + "\n";
	}
	

}
