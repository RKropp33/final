
public class Address {
	
	
	private String street;
	private String city;
	private String state;
	private int zip;
	
	
	public Address(String address)
	{
	
		
		String line = address;
		String[] tokens = line.split(",");
		this.street = tokens[0];
		this.city= tokens[1];
		this.state = tokens[2];
		this.zip = Integer.parseInt(tokens[3]);
				
		
	}
	public Address()
	{
		

		this.street = " ";
		this.city= " ";
		this.state = " ";
		this.zip = 00000 ;
		
		
	}
	
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getZip() {
		return zip;
	}
	public void setZip(int zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String toString() {
		return "Address :" + street + " " + city + " " + state + " " + zip + ".";
	}
	
	
	
	
	

}
