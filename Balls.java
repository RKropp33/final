
public class Balls extends TangibleItem{
	
	String size;
	
	public Balls(String sn, String name, String make, double price)
	{
		super(sn,name, make,price);
		this.size = "1.68 in";
		
		
	}

	@Override
	public String toString() {
		return "StockNumber=" + stockNumber + "\nname=" + name + "\nmake=" + make + "\nprice="
				+ price + "$\nsize=" + size + "\n";
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	

}
