

public class Customer {
	
	private String name;
	private String CreditcardNumber;
	private ShoppingCart cart;
	private double purchaseTotal;
	
	public Customer(String name, String cardNumber, ShoppingCart cart)
	{
		
		this.name = name;
		this.CreditcardNumber = cardNumber;
		this.cart = cart;
		purchaseTotal = cart.getCartTotal();
		
		
	}
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCreditcardNumber() {
		return CreditcardNumber;
	}


	public void setCreditcardNumber(String creditcardNumber) {
		CreditcardNumber = creditcardNumber;
	}


	public ShoppingCart getCart() {
		return cart;
	}


	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}


	public double getCartTotal() {
		return purchaseTotal;
	}


	public void setCartTotal(double cartTotal) {
		this.purchaseTotal = cartTotal;
	}


	@Override
	public String toString() {
		return "Customer [name=" + name + ", CreditcardNumber=" + CreditcardNumber + ", cartTotal=" + purchaseTotal + "]";
	}
	public String toCreditCardProcess() {
		return  name+","+CreditcardNumber+","+purchaseTotal;
	}

	
	



	
	
	

}
